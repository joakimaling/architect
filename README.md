Host netgear
  HostKeyAlgorithms=ssh-rsa
  HostName 192.168.86.1
  User root

Host aikido
  HostName ssh.loopia.se
  IdentityFile ~/.ssh/vasteraskiaikido.se
  User j3ov1c
